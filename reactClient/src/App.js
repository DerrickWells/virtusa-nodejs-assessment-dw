import React, { Component } from 'react';

class App extends Component {

  async postData() {
    try {
      let result =- await fetch('http://localhost:8080/api/v1/parse', {
        method: 'post',
        mode: 'cors',
        headers: {
          'Accept': 'application/json',
          'Content-type': 'application/json',
        },
        body: JSON.stringify({
          "data": "JOHN0000MICHAEL0009994567"
        })
      });

      console.log('Result = '+ result);

    } catch(e) {
      console.log(e);
    }
  }

  render() {
    return (
      <div className="App">
          <button onClick={ () => this.postData() }>Press me to post some data</button>
      </div>
    );
  };
}

export default App;
