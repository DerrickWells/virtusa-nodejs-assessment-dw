import {Express, Request, Response} from "express";
import express from "express";
import * as path from "path";

export class Server {

    private app: Express;

    constructor(app: Express) {
        this.app = app;

        this.app.use(express.static(path.resolve("./") + "reactClient/build"));
        this.app.use(express.json());

        this.app.post('/api/v1/parse',(req, res) => {
            // console.log("req.body.data (v1)= " + JSON.stringify(req.body.data));

            // needs error handling logs for monitoring traffic
            let firstName = JSON.stringify(req.body.data).replace(/\"/g, "").split('0000').shift() + '0000';
            let lastName = JSON.stringify(req.body.data).replace(/\"/g, "").split('0000')[1].split('000').shift() +'000';
            let clientId = JSON.stringify(req.body.data).replace(/\"/g, "").split('0000')[1].split('000').pop();

            res.json({"data": {
                  "firstName": firstName,
                  "lastName": lastName,
                  "clientId": clientId
              }});
          });

        this.app.post('/api/v2/parse',(req, res) => {
            // console.log("req.body.data (v2)= " + JSON.stringify(req.body.data));

            // needs error handling logs for monitoring traffic
            let firstName = JSON.stringify(req.body.data).replace(/\"/g, "").split('0000').shift();
            let lastName = JSON.stringify(req.body.data).replace(/\"/g, "").split('0000')[1].split('000').shift();
            let clientId = JSON.stringify(req.body.data).replace(/\"/g, "").split('0000')[1].split('000').pop();

            res.json({"data": {
                  "firstName": firstName,
                  "lastName": lastName,
                  "clientId": clientId.slice(0,3) + '-' + clientId.slice(3)
              }});
        });

        this.app.get("*", (req: Request, res: Response): void => {
            res.sendFile(path.resolve("./") + "/reactClient/build/index.html");
        });
    }

    public start(port: number): void {
        this.app.listen(port, () => console.log(`Server listening on port ${port}!`));
    }

}

