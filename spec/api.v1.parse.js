const request = require('request');
const server = require('../build/index');

const endpoint = 'http://localhost:8080';

describe('homepage', function () {
    it('should return 200 response code', function (done) {
        request.get({
            url: 'https://localhost:8080',
            json: {
                "data": "JOHN0000MICHAEL0009994567"
            }, function(err, response, body) {
                if (err) throw(err)
                expect(response.statusCode).toEqual(200);
                done();
            }
        });
    });

    it('should pass on POST', function (done) {
        request.post({
            url: 'https://localhost:8080/api/v1/parse',
            json: {
                "data": "JOHN0000MICHAEL0009994567"
            }, function(err, response, body) {
                if (err) throw(err)
                expect(response.statusCode).toEqual(200);
                done();
            }
        });
    });
});